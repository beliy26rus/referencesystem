﻿using System.Web.Mvc;

namespace ReferenceSystem.WebApi.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}