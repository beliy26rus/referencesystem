﻿using System.Collections.Generic;
using System.Web.Http;
using ReferenceSystem.DataLayer;

namespace ReferenceSystem.WebApi.Controllers
{
    public class MainController : ApiController
    {
        public ReferenceObject GetHierarchy(int id)
        {
            var engine = new EEngine();
            return engine.GetHierarchy(id);
        }

        public ReferenceObject GetObjectById(int id)
        {
            var engine = new EEngine();
            return engine.GetObjectById(id);
        }

        [HttpGet]
        public void SetPropertyValue(int objectId, string attributeName, string value)
        {
            var engine = new EEngine();
            engine.SetPropertyValue(objectId, attributeName, value);
        }

        public int CreateNewObject(ReferenceObject referenceObject)
        {
            var engine = new EEngine();
            return engine.CreateNewObject(referenceObject);
        }

        [HttpGet]
        public IEnumerable<ReferenceObject> FindObjects(string className, string condition)
        {
            var engine = new EEngine();
            return engine.FindObjects(className, condition);
        }
    }
}