﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReferenceSystem.DataLayer;

namespace ReferenceSystem.WebApi
{
    public class ReferenceObjectConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var referenceObject = value as ReferenceObject;
            if (referenceObject == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            writer.WritePropertyName($"{referenceObject.ClassName}{referenceObject.Id}");
            writer.WriteStartObject();
            if (referenceObject.Properties != null)
            {
                foreach (var prop in referenceObject.Properties)
                {
                    writer.WritePropertyName(prop.Name);
                    serializer.Serialize(writer, prop.Value);
                }
            }
            if (referenceObject.ChildObjects != null)
            {
                foreach (var child in referenceObject.ChildObjects)
                    WriteJson(writer, child, serializer);
            }
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var referenceObject = new ReferenceObject();
            JObject jsonObject = JObject.Load(reader);
            var rootProperty = jsonObject.First as JProperty;
            referenceObject.ClassName = rootProperty.Name;
            var props = rootProperty.Value;
            referenceObject.ParentId = props["ParentId"]?.Value<int?>();
            referenceObject.Properties = new List<ReferenceProperty>();
            foreach (var prop in props)
            {
                var jo = prop as JProperty;
                referenceObject.Properties.Add(new ReferenceProperty(jo.Name, (string) jo.Value));
            }
            return referenceObject;
           
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (ReferenceObject);
        }
    }

    class ListReferenceObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            if (objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof (List<>))
            {
                Type itemType = objectType.GetGenericArguments().Single();
                if (itemType.IsClass && !typeof (IEnumerable).IsAssignableFrom(itemType))
                    return true;
            }
            return false;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            IEnumerable<ReferenceObject> list = (IEnumerable<ReferenceObject>) value;
            if (list == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            if (list.Any())
            {
                writer.WriteStartArray();
                foreach (var referenceObject in list)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName($"{referenceObject.ClassName}{referenceObject.Id}");
                    writer.WriteStartObject();
                    if (referenceObject.Properties != null)
                    {
                        foreach (var prop in referenceObject.Properties)
                        {
                            writer.WritePropertyName(prop.Name);
                            serializer.Serialize(writer, prop.Value);
                        }
                    }
                    if (referenceObject.ChildObjects != null)
                    {
                        foreach (var child in referenceObject.ChildObjects)
                            WriteJson(writer, child, serializer);
                    }
                    writer.WriteEndObject();
                    writer.WriteEndObject();
                }
                writer.WriteEndArray();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}