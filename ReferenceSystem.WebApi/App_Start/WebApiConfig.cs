﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ReferenceSystem.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var jsonSettings = config.Formatters.JsonFormatter.SerializerSettings;
            jsonSettings.Converters.Add(new ReferenceObjectConverter());
            jsonSettings.Converters.Add(new ListReferenceObjectConverter());
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            config.Routes.MapHttpRoute(
                name: "SetPropertyRoute",
                routeTemplate: "api/{controller}/{action}/{objectId}/{attributeName}/{value}",
                defaults: new {}
                );

            config.Routes.MapHttpRoute(
                name: "FindObjectsRoute",
                routeTemplate: "api/{controller}/{action}/{className}/{condition}",
                defaults: new { }
                );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new {id = RouteParameter.Optional}
                );
        }
    }
}