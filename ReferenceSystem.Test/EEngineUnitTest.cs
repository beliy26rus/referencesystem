﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReferenceSystem.DataLayer;

namespace ReferenceSystem.Test
{
    [TestClass]
    public class EEngineUnitTest
    {
        [TestMethod]
        public void CountConditionTest()
        {
            string condition = "Масса>120;Марка=8;Дата установки=12/02/2014";
            var engine = new EEngine();
            int count;
            engine.GetSqlQueryCondition(condition,"Насос", out count);
            Assert.AreEqual(3, count);
        }

        [TestMethod]
        public void GenerateSqlConditionTest()
        {
            string condition = "Масса>120;Марка=8;Дата установки=12/02/2014";
            var engine = new EEngine();
            int count;
            var sqlCondition = engine.GetSqlQueryCondition(condition,"Насос", out count);
            Assert.AreEqual(sqlCondition, "((ta.Name='Масса' AND ISNUMERIC(tp.[Value]) = 1 AND tp.[Value] > 120) OR (ta.Name='Марка' AND tp.[Value] = '8') OR (ta.Name='Дата установки' AND ISDATE(tp.[Value]) = 1 AND CONVERT(DateTime, tp.[Value], 113) = CONVERT(DateTime, '12/02/2014', 113)))");

        }
        

    }
}
