﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ReferenceSystem.DataLayer
{
    public class EEngine
    {
        public int CreateNewObject(ReferenceObject referenceObject)
        {
            int id;
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("CreateNewObject", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@class", referenceObject.ClassName);
                    if (referenceObject.ParentId != null)
                        command.Parameters.AddWithValue("@parentId", referenceObject.ParentId);
                    var idParam = new SqlParameter("@newId", SqlDbType.Int);
                    idParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(idParam);
                    command.Parameters.AddWithValue("@properties", PropertiesToTableType(referenceObject.Properties));
                    command.ExecuteNonQuery();
                    id = (int) idParam.Value;
                }
                connection.Close();
            }
            return id;
        }

        public ReferenceObject GetHierarchy(int id)
        {
            var objects = new List<ReferenceObject>();
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("GetHierarchy", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@objectId", id);
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        objects.Add(new ReferenceObject
                        {
                            Id = reader.GetInt32(reader.GetOrdinal("ObjectId")),
                            ClassName = reader.GetString(reader.GetOrdinal("Name")),
                            ParentId = reader.IsDBNull(reader.GetOrdinal("ParentObjectId"))
                                ? (int?) null
                                : reader.GetInt32(reader.GetOrdinal("ParentObjectId"))
                        });
                    }
                }
                connection.Close();
            }
            var referenceObject = objects.First();
            AddChild(referenceObject, objects);
            return referenceObject;
        }

        public ReferenceObject GetObjectById(int id)
        {
            var referenceObject = new ReferenceObject();
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("GetObjectById", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@objectId", id);
                    var parentObjectIdparam = new SqlParameter("@parentObjectId", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };
                    var classNameParam = new SqlParameter("@className", SqlDbType.VarChar, 100)
                    {
                        Direction = ParameterDirection.Output
                    };
                    command.Parameters.Add(parentObjectIdparam);
                    command.Parameters.Add(classNameParam);
                    var reader = command.ExecuteReader();
                    referenceObject.Properties = new List<ReferenceProperty>();
                    while (reader.Read())
                    {
                        referenceObject.Properties.Add(new ReferenceProperty(
                            reader.GetString(reader.GetOrdinal("AttributeName")),
                            reader.GetString(reader.GetOrdinal("Value")),
                            reader.GetString(reader.GetOrdinal("TypeName"))
                            ));
                    }
                    connection.Close();
                    referenceObject.Id = id;
                    referenceObject.ClassName = (string) classNameParam.Value;
                    referenceObject.ParentId = (int?) parentObjectIdparam.Value;
                }
            }
            return referenceObject;
        }

        public void SetPropertyValue(int objectId, string attributeName, string value)
        {
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("SetPropertyValue", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@objectId", objectId);
                    command.Parameters.AddWithValue("@attributeName", attributeName);
                    command.Parameters.AddWithValue("@value", value);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public List<ReferenceObject> FindObjects(string className, string condition)
        {
            var queryparam = new SqlParameter("@q", SqlDbType.VarChar,9999);
            queryparam.Direction = ParameterDirection.Output;
            int count;
            string sqlCondition = GetSqlQueryCondition(condition, className, out count);
            var objectHelpers = new List<ReferenceObjectHelper>();
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("FindObject", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@className", className);
                    command.Parameters.AddWithValue("@condition", sqlCondition);
                    command.Parameters.AddWithValue("@count", count);
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        objectHelpers.Add(new ReferenceObjectHelper()
                        {
                            ObjectId = reader.GetInt32(reader.GetOrdinal("ObjectId")),
                            ClassName = reader.GetString(reader.GetOrdinal("Name")),
                            ParentObjectId =
                                reader.IsDBNull(reader.GetOrdinal("ParentObjectId"))
                                    ? (int?)null
                                    : reader.GetInt32(reader.GetOrdinal("ParentObjectId")),
                            AttributeName = reader.GetString(reader.GetOrdinal("AttributeName")),
                            Value = reader.GetString(reader.GetOrdinal("Value"))
                        });
                    }
                }
                connection.Close();
            }
            return objectHelpers.GroupBy(item => item.ObjectId).Select(item => new ReferenceObject()
            {
                Id = item.Key,
                ClassName = item.First().ClassName,
                ParentId = item.First().ParentObjectId,
                Properties = item.Select(g => new ReferenceProperty(g.AttributeName, g.Value)).ToList()
            }).ToList();
        }

        private void AddChild(ReferenceObject referenceObject, List<ReferenceObject> objects)
        {
            var childObjects = objects.Where(item => item.ParentId == referenceObject.Id).ToList();
            foreach (var childobject in childObjects)
                AddChild(childobject, objects);
            referenceObject.ChildObjects = childObjects.ToList();
        }

        private DataTable PropertiesToTableType(List<ReferenceProperty> properties)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("AttributeName", typeof (string));
            dataTable.Columns.Add("Value", typeof (string));
            foreach (var prop in properties)
                dataTable.Rows.Add(prop.Name, prop.Value);
            return dataTable;
        }

        private Dictionary<string, string> GetAttribiteTypes(string className)
        {
            var attributeDictionary = new Dictionary<string, string>();
            var conectionString = ConfigurationManager.ConnectionStrings["EContext"].ConnectionString;
            using (var connection = new SqlConnection(conectionString))
            {
                connection.Open();
                using (var command = new SqlCommand("GetAttribiteTypes", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@className", className);
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        attributeDictionary.Add(reader.GetString(reader.GetOrdinal("AttributeName")),
                            reader.GetString(reader.GetOrdinal("TypeName")));
                    }
                }
                connection.Close();
            }

            return attributeDictionary;
        }

        public string GetSqlQueryCondition(string condition, string className, out int count)
        {
            try
            {
                var attributeDictionary = GetAttribiteTypes(className);
                count = 0;
                StringBuilder sqlCondition = new StringBuilder();
                sqlCondition.Append("(");
                condition = condition.Replace("'", "''");
                var simpleConditions = condition.Split(';');
                for (int i = 0; i < simpleConditions.Length; i++)
                {
                    count++;
                    string simplesqlCondition = String.Empty;
                    var con = simpleConditions[i];
                    var operands = con.Split(new[] {">", "<", "=", "!="}, StringSplitOptions.RemoveEmptyEntries);
                    var propertyName = operands[0];
                    var propertyValue = operands[1];
                    var conditionOperator = con.Replace(propertyName, "").Replace(propertyValue, "");
                    var type = attributeDictionary[propertyName];
                    switch (type)
                    {
                        case "String":
                            simplesqlCondition =
                                $"(ta.Name='{propertyName}' AND tp.[Value] {conditionOperator} '{propertyValue}')";
                            break;
                        case "Float":
                            simplesqlCondition =
                                $"(ta.Name='{propertyName}' AND ISNUMERIC(tp.[Value]) = 1 AND tp.[Value] {conditionOperator} {propertyValue})";
                            break;
                        case "DateTime":
                            simplesqlCondition =
                                $"(ta.Name='{propertyName}' AND ISDATE(tp.[Value]) = 1 AND CONVERT(DateTime, tp.[Value], 113) {conditionOperator} CONVERT(DateTime, '{propertyValue}', 113))";
                            break;
                    }

                    sqlCondition.Append(simplesqlCondition);
                    if (i + 1 != simpleConditions.Length)
                        sqlCondition.Append(" OR ");
                }
                sqlCondition.Append(")");

                return sqlCondition.ToString();
            }
            catch
            {
                throw new Exception("Wrong search mask");
            }
        }
    }
}