﻿namespace ReferenceSystem.DataLayer
{
    public class ReferenceObjectHelper
    {
        public int ObjectId { get; set; }

        public string ClassName { get; set; }

        public int? ParentObjectId { get; set; }

        public string AttributeName { get; set; }

        public string Value { get; set; }
    }
}
