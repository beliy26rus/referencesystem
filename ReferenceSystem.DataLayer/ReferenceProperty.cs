﻿namespace ReferenceSystem.DataLayer
{
    public class ReferenceProperty
    {
        public ReferenceProperty(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public ReferenceProperty(string name, string value, string type)
        {
            Name = name;
            Value = value;
            Type = type;
        }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Type { get; set; }
    }
}
