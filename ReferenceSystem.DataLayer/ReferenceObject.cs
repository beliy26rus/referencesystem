﻿using System.Collections.Generic;

namespace ReferenceSystem.DataLayer
{
  
    public class ReferenceObject
    {
        public int Id { get; set; }

        public string ClassName { get; set; }

        public int? ParentId { get; set; }

        public List<ReferenceProperty> Properties { get; set; }

        public List<ReferenceObject> ChildObjects { get; set; }
    }
}