﻿using System;
using System.Collections.Generic;
using ReferenceSystem.DataLayer;

namespace ReferenceSystem.Console
{
    internal class Program
    {
        /// <summary>
        /// Добавление тестовых данных
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var total = 0;
            var random = new Random();
            while (total <= 1000000)
            {
                var parentIds = new List<int?> {null};
                var deep = 0;
                while (deep <= 10)
                {
                    var list = new List<int?>();
                    foreach (var parentId in parentIds)
                    {
                        var count = random.Next(1, 4);
                        for (var i = 0; i < count; i++)
                        {
                            list.Add(CreateObject(parentId, random));
                            total++;
                            System.Console.WriteLine(total);
                        }
                    }
                    deep++;
                    parentIds = list;
                }
            }
        }

        private static int CreateObject(int? parentId, Random random)
        {
            var referenceObject = new ReferenceObject
            {
                ParentId = parentId
            };
            var objectType = random.Next(1, 4);
            switch (objectType)
            {
                case 1:
                    referenceObject.ClassName = "Насос";
                    referenceObject.Properties = new List<ReferenceProperty>
                    {
                        new ReferenceProperty("Масса", $"{random.Next(10, 50)}"),
                        new ReferenceProperty("Дата установки", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")),
                        new ReferenceProperty("Марка", $"ГОСТ{random.Next(10, 99999)}"),
                        new ReferenceProperty("Напор", $"{random.Next(10, 50)}")
                    };
                    break;

                case 2:
                    referenceObject.ClassName = "Труба";
                    referenceObject.Properties = new List<ReferenceProperty>
                    {
                        new ReferenceProperty("Длина", $"{random.Next(10, 50)}"),
                        new ReferenceProperty("Марка", $"ГОСТ Т{random.Next(10, 99999)} - A"),
                        new ReferenceProperty("Диаметр", $"{random.Next(10, 50)}")
                    };
                    break;
                case 3:
                    referenceObject.ClassName = "Задвижка";
                    referenceObject.Properties = new List<ReferenceProperty>
                    {
                        new ReferenceProperty("Масса", $"{random.Next(10, 40)}"),
                        new ReferenceProperty("Дата установки", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")),
                        new ReferenceProperty("Диаметр", $"{random.Next(10, 50)}"),
                        new ReferenceProperty("Наименование", $"30с{random.Next(10, 50)}нж")
                    };
                    break;
            }
            var engine = new EEngine();
            return engine.CreateNewObject(referenceObject);
        }
    }
}